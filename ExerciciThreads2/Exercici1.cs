﻿using System;

namespace ExerciciThreads2
{
    class Exercici1
    {
        /*public static void Main(string[] args)
        {
            Thread thread1 = new Thread(() =>
            {
                LlegirText("Una vegada hi havia un gat");
            });

            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                LlegirText("En un lugar de la Mancha");
            });

            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                LlegirText("Once upon a time in the west");
            });

            thread1.Start();
            thread2.Start();   
            thread3.Start();
        }*/

        public static void LlegirText(string line)
        {
            string[] frase = line.Split(' ');

            for (int i = 0; i < frase.Length; i++)
            {
                Thread.Sleep(1000);
                Console.Write(frase[i] + " ");
            }

            Console.WriteLine();
        }
    }
}