﻿using System;
namespace ExerciciThreads2
{
    class Nevera
    {
        private int cerveses;

        public Nevera()
        {
            cerveses = 6;
        }

        public void omplirNevera(string persona)
        {
            int cervesesAfegides = new Random().Next(7);

            if (cerveses + cervesesAfegides > 9) cerveses = 9;
            else cerveses += cervesesAfegides;

            Console.WriteLine(persona + " ha afegit " + cervesesAfegides + " cerveses a la nevera.");
            Console.WriteLine("La nevera té ara " + cerveses + " cerveses.");
        }

        public void beureCervesa(string persona)
        {
            int cervesesBegudes = new Random().Next(7);

            if (cerveses - cervesesBegudes < 0) cerveses = 0;
            else cerveses -= cervesesBegudes;

            Console.WriteLine(persona + " ha begut " + cervesesBegudes + " cerveses de la nevera.");
            Console.WriteLine("La nevera té ara " + cerveses + " cerveses.");
        }
    }

    class Test
    {
        public static void Main(string[] args)
        {
            Nevera nevera = new Nevera();

            Thread AnittaThread = new Thread(() => nevera.omplirNevera("Anitta"));
            Thread BadBunnyThread = new Thread(() => nevera.beureCervesa("Bad Bunny"));
            Thread LilNasXThread = new Thread(() => nevera.beureCervesa("Lil Nas X"));
            Thread ManuelTurizoThread = new Thread(() => nevera.beureCervesa("Manuel Turizo"));

            AnittaThread.Start();
            AnittaThread.Join();

            BadBunnyThread.Start();
            LilNasXThread.Start();
            ManuelTurizoThread.Start();

            BadBunnyThread.Join();
            LilNasXThread.Join();
            ManuelTurizoThread.Join();
        }
    }
} 
